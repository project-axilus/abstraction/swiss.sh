# [swiss.sh][linkedin]

_Creating a pocket sized toolkit library for any situation._

## Purpose

Hacking together libraries from all over the place can be tiring.
Also, the knowledge gained from the creation of a library is beneficial.
Both of these factors have lead to the development of these utilities:

- **Log** displays formatted info, warn, error, fatal, and trace messages.
- **Map** runs a function over a list of arguments.
- **Test** provides simple functions for creating unit tests.

## Usage

First, clone/download swiss to your local directory, and create a test script:

```sh
echo "\
#! /bin/bash

source swiss/swiss.sh

swiss::log::info 'Hello swiss\!'
" > hello_swiss.sh
```

Finally, make your script executable, and run it:

```sh
chmod +x hello_swiss.sh
./hello_swiss.sh
```

Congratulations, you've now imported, and utilized the swiss.sh library.
Now go forth, and be awesome.

## License

Copyright © Mr Axilus.
This project is licensed under [CC BY-NC-SA 4.0][license].

[linkedin]: https://www.linkedin.com/in/mraxilus
[license]: https://creativecommons.org/licenses/by-nc-sa/4.0/
